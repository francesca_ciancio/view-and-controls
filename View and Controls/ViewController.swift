//
//  ViewController.swift
//  View and controls
//
//  Created by FrancescaCiancio on 07/11/2019.
//  Copyright © 2019 FrancescaCiancio. All rights reserved.
//

import UIKit
//the app looks nice on iPhone 8 and SE
class ViewController: UIViewController , UITextFieldDelegate {
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    
    @IBOutlet weak var redSwitch: UISwitch!
    @IBOutlet weak var greenSwitch: UISwitch!
    @IBOutlet weak var blueSwitch: UISwitch!
    
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    
    @IBOutlet var stepper: UIStepper!
    @IBOutlet var sizeValue: UILabel!
    var color: UIColor!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
        colorView.layer.borderWidth = 5
        colorView.layer.cornerRadius = 20
        colorView.layer.borderColor = UIColor.black.cgColor
        setUpStart()
        
    }
    
    func setUpStart() {
        colorView.backgroundColor = .black
        text.text = ""
        text.font = UIFont.systemFont(ofSize: 17.0)
        textField.text = "Text"
        sizeValue.text = "17.0"
        stepper.value = 17
        redSlider.value = 1
        greenSlider.value = 1
        blueSlider.value = 1
        redSwitch.isOn = false
        greenSwitch.isOn = false
        blueSwitch.isOn = false
        updateControls()
    }
    
    @IBAction func switchChanged(_ sender: UISwitch) {
        updateColor()
        updateControls()
    }
    //    this function dismiss the keyboard by clicking the send(return) button on it
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        text.text = textField.text
        return true
    }
    //    this function dismiss the keyboard by touching on the white screen outside the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    func updateColor() {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        if redSwitch.isOn{
            red = CGFloat(redSlider.value)
        }
        if greenSwitch.isOn{
            green = CGFloat(greenSlider.value)
        }
        if blueSwitch.isOn{
            blue = CGFloat(blueSlider.value)
        }
        color = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
        colorView.backgroundColor = color
    }
    //    this function enable or disable the slider when the respective switch is enable or not
    func updateControls(){
        redSlider.isEnabled = redSwitch.isOn
        greenSlider.isEnabled = greenSwitch.isOn
        blueSlider.isEnabled = blueSwitch.isOn
    }
    
    @IBAction func sliderChanged(_ sender: Any) {
        updateColor()
    }
    
    @IBAction func reset(_ sender: Any) {
        setUpStart()
    }
    
    
    //    this function upDate the size on the text
    @IBAction func stepperAction(_ sender: UIStepper) {
        sizeValue.text = String(sender.value)
        text.font = UIFont.systemFont(ofSize: CGFloat(sender.value))
        
    }
}

